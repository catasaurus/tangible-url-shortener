FROM python:3.9.12

WORKDIR /usr/src/urlshortener

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
COPY . .
RUN pip install .